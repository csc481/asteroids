define(['../engine/Mochi'], function(Mochi) {

    /**
     * This component is used to direct ship controls
     */
    class ShipControl extends Mochi.Component {
        /**
         * Creates a ship controller for an entity. Requires an InputManager
         * for bindings.
         * @param input The Input object.
         */
        constructor(input, bulletFactory, width, height) {
            super();

            this.input = input;
            this.bulletFactory = bulletFactory;
            this.width = width;
            this.height = height;

            this.turnSpeed = Math.PI;
            this.acceleration = 5;
            this.maxSpeed = 15;

            this.fireTime = .3;
            this.elapsedTime = 1;
        }

        /**
         * Invoked when the component has been attached to an entity.
         * @param entity The entity that this component is attached to
         */
        init(entity) {
            this.entity = entity;
        }

        turnLeft(dt) {
            this.entity.turn(this.turnSpeed * dt);
        }

        turnRight(dt) {
            this.entity.turn(-this.turnSpeed * dt);
        }

        accelerate(dt) {
            this.entity.body.speedX += this.acceleration * Math.cos(this.entity.rotation) * dt;
            this.entity.body.speedY += this.acceleration * Math.sin(this.entity.rotation) * dt;
            let speed = Math.sqrt(Math.pow(this.entity.body.speedX, 2) + Math.pow(this.entity.body.speedY, 2));
            if (speed > this.maxSpeed) {
                this.entity.body.speedX *= this.maxSpeed / speed;
                this.entity.body.speedY *= this.maxSpeed / speed;
            }
        }

        decelerate(dt) {
            let speed = Math.sqrt(Math.pow(this.entity.body.speedX, 2) + Math.pow(this.entity.body.speedY, 2));
            if (speed === 0) {
                return;
            }

            let xSign = this.entity.body.speedX < 0 ? -1 : 1;
            this.entity.body.speedX *= (speed - (this.acceleration * dt)) / speed;
            if ((this.entity.body.speedX < 0 ? -1 : 1) !== xSign) {
                this.entity.body.speedX = 0;
            }

            let ySign = this.entity.body.speedY < 0 ? -1 : 1;
            this.entity.body.speedY *= (speed - (this.acceleration  * dt)) / speed;
            if ((this.entity.body.speedY < 0 ? -1 : 1) !== ySign) {
                this.entity.body.speedY = 0;
            }

        }

        fire() {
            if (this.elapsedTime >= this.fireTime) {
                let bullet = this.bulletFactory.make(this.entity.x, this.entity.y, this.entity.rotation, this.width, this.height);
                this.entity.events.broadcast("shoot", bullet);
                this.elapsedTime = 0;
            }
        }

        update(dt) {
            this.elapsedTime += dt;
            if ('a' in this.input.keysDown) {
                this.turnLeft(dt);
            }
            if ('d' in this.input.keysDown) {
                this.turnRight(dt);
            }
            if ('w' in this.input.keysDown) {
                this.accelerate(dt);
            }
            if ('s' in this.input.keysDown) {
                this.decelerate(dt);
            }
            if (' ' in this.input.keysDown) {
                this.fire();
            }
        }

    }
    // The name of the ship component within mochi
    ShipControl.Name = "shipControl";

    return ShipControl;
});