define(['../engine/Mochi'], function(Mochi) {

    /**
     * Class that keeps track of the game's score.
     */
    class Score {
        constructor(x, y) {
            this.x = x;
            this.y = y;
            this.value = 0;
        }

        /** Loads the score from local JS storage */
        load() {
            var score = Number(localStorage.getItem("asteroids_score"));
            if (score === null) {
                // Initialize the value as 0.
                localStorage.setItem("asteroids_score", 0);
                return;
            }
        }

        /** Saves the score from local JS storage. Returns true if high score. */
        save() {
            var stored = Number(localStorage.getItem("asteroids_score"));
            if (stored != null && this.value > stored) {
                localStorage.setItem("score", this.value);
                return true;
            }
            return false;
        }

        /** Draws the score */
        draw(ctx) {
            ctx.font = '30px serif';
            ctx.fillStyle = 'black';
            ctx.textAlign="start";
            ctx.fillText('Score: ' + this.value, this.x, this.y);
        }

        drawHighScore(ctx, x, y) {
            ctx.font = '30px serif';
            ctx.fillStyle = 'black';
            ctx.textAlign="center";
            ctx.fillText('Game Over! You have a high score! Enter to restart.', x, y);
        }

        drawGameOver(ctx, x, y) {
            ctx.font = '30px serif';
            ctx.fillStyle = 'black';
            ctx.textAlign="center";
            ctx.fillText('Game Over! No high score! Enter to restart.', x, y);
        }
    }
    Score.HEIGHT = 20;


    return Score;
});