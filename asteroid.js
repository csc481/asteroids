define(['../engine/Mochi', './bounds'], function(Mochi, Bounds) {
    /**
     * Adds astroid behaviors to an object.
     */
    class Asteroid extends Mochi.Component{

        constructor(rotation, width, height){
            super();
            this.rotation = rotation;
            this.speed = 5;

            this.width = width;
            this.height = height;
        }

        init(entity) {
            this.entity = entity;

            this.entity.rotation = this.rotation;
            this.entity.body.speedX = this.speed * Math.cos(this.rotation);
            this.entity.body.speedY = this.speed * Math.sin(this.rotation);
        }

    }
    Asteroid.Name = "asteroid";

    /**
     * Creates asteroids similar to a particle system. Will be replaced in refactor by a particle system.
     */
    class AsteroidFactory {
        /**
         * Creates a new ParticleSystem for asteroids
         * @param spawnRate How fast particles spawn
         * @param scene The scene to add particles to.
         */
        constructor(spawnRate, main) {
            this.meteor = new Mochi.Content.Asset("assets/testAnimation3.png", Mochi.Content.Types.image);
            // random modifiers
            this.defaultSize = 1;
            this.defaultRot = 0;
            this.main = main;
        }


        getAssets() {
            return [this.meteor];
        }

        make() {
            let game = this.main.game;
            let asteroid = new Mochi.Entity(0, 0);
            //asteroid.add(new Mochi.Graphics.Sprite(this.meteor, { width: 96, height: 32 }));
            let size = this.defaultSize;
            asteroid.add(new Mochi.Graphics.AnimatedSprite(this.meteor, {
                width: size, height: size, frameWidth: 32, animationHeight:32, frameTotal: 2, animationTotal: 2,
                tick: 20, anchor: {x: .5, y: .5}
            }));
            asteroid.get('sprite').switchAnimations(1);
            // radius is based on size
            let radius = size / 2;
            asteroid.add(new Mochi.Physics.Body(new Mochi.Physics.Shape.Circle(radius)));
            asteroid.add(new Asteroid(0, game.width, game.height));
            asteroid.add(new Bounds.Handler(game.width, game.height));
            asteroid.add(new Bounds.PositionResolve());
            asteroid.init();
            return asteroid;
        }
    }

    /** Creates an asteroid spawner */
    function makeAsteroidSpawner(game, factory, spawnRate) {
        let entity = new Mochi.Entity(game.width/2, game.height/2);
        entity.add(new Mochi.Physics.Emitter(
            // archetype of the object
            {
                // optional specifier for variation amt, otherwise uses what already exists from factory
                x: new Mochi.Physics.Properties.Range(0, game.width),
                y: new Mochi.Physics.Properties.Range(0, game.height),
                rotation: new Mochi.Physics.Properties.Range(0, 2 * Math.PI),
                body: {
                    speedX: new Mochi.Physics.Properties.Range(-5, 5),
                    speedY: new Mochi.Physics.Properties.Range(-5, 5),
                    shape: {
                        size: new Mochi.Physics.Properties.Custom(new Mochi.Physics.Properties.Range(1, 3), function (entity, parent, generated) {
                            entity.body.shape.radius = generated / 2;
                            entity.sprite.width = generated;
                            entity.sprite.height = generated;
                        })
                    }
                }
            },
            // configuration setters of the emitter
            {
                //spawnCount: new Mochi.Physics.Properties.Range(0, 2),
                // required set to 3 with 20% variation off of 3
                spawnTime: new Mochi.Physics.Properties.Variation(.3, spawnRate),
                //spawnMax: 10,
                // optional decay
                //decayTime: new Variation(.4),
            },
            factory
        ));
        entity.init();
        return entity;
    }

    return {
        Asteroid: Asteroid,
        Factory: AsteroidFactory,
        MakeSpawner: makeAsteroidSpawner
    };
});